🌞 **Installer MariaDB sur la machine `db.tp5.linux`**

```bash

[ed12@db ~]$ sudo dnf install mariadb-server  
    Complete!
```

🌞 **Le service MariaDB**
```bash

[ed12@db ~]$ sudo systemctl start mariadb

[ed12@db ~]$ sudo systemctl enable mariadb   
    Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service. 
    Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
    Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

[ed12@db ~]$ systemctl status mariadb
     Active: active (running) since Thu 2021-11-25 17:09:36 CET; 3min 16s ago

[ed12@db ~]$ sudo ss -ltpn  
    LISTEN   0        80                     *:3306                *:*      users:(("mysqld",pid=8910,fd=21))

[ed12@db ~]$ ps -aux | grep mariadb 
    ed12        9062  0.0  0.1 221928  1192 pts/0    S+   17:17   0:00 grep --color=auto mariadb

```
🌞 **Firewall**

```bash
[ed12@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent 
    [sudo] password for ed12:
    success 

[ed12@db ~]$ sudo firewall-cmd --reload 
    success
```
## 2. Conf MariaDB
```bash

-Q1 :current pswd for root 
    je laisse blanc car je n'ai encore rien conf for root

-Q2 :Set root password? [Y/n] 
    Je def un mdp bidon (genre 123456) qui me sera demandé pour exec le service

-Q3 : 


```

🌞 Installez sur la machine web.tp5.linux la commande mysql

```bash
[ed12@web ~]$ dnf provides mysql  
    Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7  



[ed12@web ~]$ sudo dnf install mysql
    Installed:
    mariadb-connector-c-config-3.1.11-2.el8_3.noarch
    mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
    mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 
```

🌞 Tester la connexion

```bash

[ed12@web ~]$ mysql -h 10.5.1.12 -P 3306 -u nextcloud -p nextcloud
    Enter password:
    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 24
    Server version: 5.5.5-10.3.28-MariaDB MariaDB Server 

mysql> SHOW TABLES;
    Empty set (0.01 sec)
````


# II. Setup Web

## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp5.linux`**


```bash
[ed12@web ~]$ sudo dnf install httpd
    Complete! 
```

🌞 **Analyse du service Apache**

```bash

[ed12@web ~]$ systemctl start httpd
    Password: 
    ==== AUTHENTICATION COMPLETE ==== 

[ed12@web ~]$ systemctl enable httpd
    Password:
    ==== AUTHENTICATION COMPLETE ==== 
    Authentication is required to reload the systemd state.
    Authenticating as: ed12
    Password: 

[ed12@web ~]$ ps -aux | grep httpd 
    root        6616  0.0  1.3 280220 11368 ?        Ss   18:46   0:00 /usr/sbin/httpd -DFOREGROUND
    apache      6617  0.0  1.0 294104  8476 ?        S    18:46   0:00 /usr/sbin/httpd -DFOREGROUND
    apache      6618  0.0  1.4 1351892 12268 ?       Sl   18:46   0:00 /usr/sbin/httpd -DFOREGROUND 
    apache      6619  0.0  1.4 1351892 12268 ?       Sl   18:46   0:00 /usr/sbin/httpd -DFOREGROUND
    apache      6620  0.0  1.7 1483020 14316 ?       Sl   18:46   0:00 /usr/sbin/httpd -DFOREGROUND
    ed12        6881  0.0  0.1 221928  1156 pts/1    S+   18:49   0:00 grep --color=auto httpd 

[ed12@web ~]$ sudo ss -lntp 
    LISTEN       0            128                            *:80                            *:*           users:(("httpd",pid=6620,fd=4),("httpd",  pid=6619,fd=4),("httpd",pid=6618,fd=4),("httpd",pid=6616,fd=4))
```
Port par defaut: 80
User qui lance: apache

🌞 **Un premier test**
```bash

[ed12@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
    success 

[ed12@web ~]$ sudo firewall-cmd --reload  
    success 

PS C:\Users\erwan> curl 10.5.1.11:80 
    curl : HTTP Server Test Page 
    This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system. If you can read this page, it means that the software it working correctly.
    Just visiting?

```

### B. PHP

🌞 **Installer PHP**

```bash
[ed12@web ~]$ sudo dnf install epel-release 
   Complete!

[ed12@web ~]$ sudo dnf update 
    Dependencies resolved.
    Nothing to do.
    Complete! 

[ed12@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm 
    Complete!

[ed12@web ~]$ sudo dnf module enable php:remi-7.4 
    Complete! 

[ed12@web ~]$ dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
    Complete! 
```
## 2. Conf Apache

🌞 Analyser la conf Apache
```bash
[ed12@web ~]$ cat /etc/httpd/conf/httpd.conf 

    Load config files in the "/etc/httpd/conf.d" directory, if any.
    IncludeOptional conf.d/*.conf 
```

🌞 **Créer un VirtualHost qui accueillera NextCloud**
```bash
[ed12@web conf.d]$ sudo nano myfile.conf 

<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/  

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp5.linux  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>

[ed12@web conf.d]$ systemctl restart httpd
    Authentication is required to restart 'httpd.service'.
    Authenticating as: ed12
    Password:
```

🌞 Configurer la racine web

```bash
[ed12@web ~]$ cd /var/www/ 
[ed12@web www]$ sudo mkdir nextcloud
[ed12@web www]$ cd nextcloud/ 
[ed12@web nextcloud]$ sudo mkdir html
[ed12@web nextcloud]$ ls 
    html 

[ed12@web nextcloud]$ sudo chown -R apache /var/www/nextcloud/html/ 
```

🌞 **Configurer PHP**

```bash
[ed12@web ~]$ sudo nano /etc/opt/remi/php74/php.ini 
    [Date]
    ; Defines the default timezone used by the date functions
    ; http://php.net/date.timezone
    ;date.timezone = "Europe/Paris" 
```
## 3. Install NextCloud

🌞 **Récupérer Nextcloud**

```bash
[ed12@web ~]$ cd 
[ed12@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip 
[ed12@web ~]$ ls 
    nextcloud-21.0.1.zip 
```

🌞 **Ranger la chambre**

```bash
[ed12@web ~]$ unzip nextcloud-21.0.1.zip
[ed12@web ~]$ sudo mv nextcloud/* /var/www/nextcloud/html 
[ed12@web ~]$ cd /var/www/nextcloud/html/ 
[ed12@web html]$ ls 
3rdparty  AUTHORS  console.php  core      index.html  lib  ocm-provider  ocs-provider  remote.php  robots.txt  themes   version.php                                                                                  apps      config   COPYING      cron.php  index.php   occ  ocs           public.php    resources   status.php  updater   

[ed12@web html]$ sudo chown -R apache:apache /var/www/nextcloud 
[ed12@web html]$ sudo systemctl restart httpd
[ed12@web html]$ cd 
[ed12@web ~]$ rm -rf nextcloud* 
```

## 4. Test

🌞 **Modifiez le fichier `hosts` de votre PC**

```bash
[ed12@web ~]$ cat /etc/hosts 
    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
    10.5.1.11  web.tp5.linux 
```

🌞 **Tester l'accès à NextCloud et finaliser son install'**

```bash
[ed12@web ~]$ curl http://web.tp5.linux 
    <!DOCTYPE html>  
    <html>
    <html>
            <script> window.location.href="index.php"; </script> 
            <meta http-equiv="refresh" content="0; URL=index.php">
    </head> 
    </html> 
```
Depuis google

Vue qu'il faut pas mettre de capture d'ecran je met juste l'url de quand je suis sur mon cloud = http://web.tp5.linux/index.php/apps/dashboard

















