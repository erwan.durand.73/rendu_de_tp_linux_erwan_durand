# TP

- [TP1 : Are you dead yet ?]

## Compte-rendu.

**Commande 1** : Fork Bomb
:(){ :|:& };:

- Cette commande a pour but de créer des process indéfiniment, ce qui aura pour but saturer la mémoire ou le CPU et ferra crash le PC.

**Commande 2** 
kill -9 -1

- La ligne de commande sert a stopper l'execution de tous les process en cours d'execution sur la machine, ce qui vas la faire crash. Cependant la machine redemarre sans probleme.

**Commande 3**
cd /home/erwan
ls -a
nano .bashrc 
sleep 20
- On de dirige dans le repertoir /home/erwan (mon repertoire personnel).
- On liste les fichiers accessibles dans ce repertoire ainsi que les fichiers chchés.
- On ouvre le dossier bashrc qui est exec a chaque fois que l'utilisateur ouvre un nouveau shell.
- Ajout de la commande sleep 20 qui ralentit énormément le systeme. A chaque fois que l'usilisateur ouvre un terminal shell ou execute un commande bash, il doit attendre 20 secondes.

**Commande 4**
rm -rf /boot

- Cette fonction supprime tout les partions de boot ce qui empeche totalement l'OS de fonctionner. 

**Commande 5**
umount -l -f /

-Demonte toute l'arborescence des fichiers a la racine.


