# Partie 1 : Préparation de la machine `backup.tp6.linux`

🌞 **Ajouter un disque dur de 5Go à la VM `backup.tp6.linux`**

```bash
[ed12@backup ~]$ lsblk | grep sdb
sdb           8:16   0    5G  0 disk
```

# II. Partitioning

🌞 **Partitionner le disque à l'aide de LVM**

```bash
[ed12@backup ~]$ sudo pvcreate /dev/sdb 
  Physical volume "/dev/sdb" successfully created.

[ed12@backup ~]$ sudo pvs  
  PV         VG Fmt  Attr PSize  PFree 
  /dev/sda2  rl lvm2 a--  <7.00g    0   
  /dev/sdb      lvm2 ---   5.00g 5.00g 

[ed12@backup ~]$ sudo pvdisplay  
  "/dev/sdb" is a new physical volume of "5.00 GiB"  
  --- NEW Physical volume ---  
  PV Name               /dev/sdb 
  PV Size               5.00 GiB 

[ed12@backup ~]$ sudo vgcreate backup /dev/sdb 
  Volume group "backup" successfully created 

[ed12@backup ~]$ sudo lvcreate -l 100%FREE backup -n lv_backup 
  Logical volume "lv_backup" created. 

[ed12@backup ~]$ sudo lvs 
  lv_backup backup -wi-a-----  <5.00g  

[ed12@backup ~]$ sudo lvdisplay 
  LV Path                /dev/backup/lv_backup 
  LV Name                lv_backup 
  VG Name                backup
  LV UUID                0moWrX-TPxi-7Nyg-cBdY-u2Iz-yDCc-oZ42gU 
  LV Write Access        read/write
  LV Creation host, time backup.tp1.linux, 2021-12-07 17:19:11 +0100
  LV Status              available
  # open                 0
  LV Size                <5.00 GiB
  Current LE             1279
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto 
  - currently set to     8192 
  Block device           253:2    
```
🌞 **Formater la partition**

```bash
[ed12@backup ~]$ sudo mkfs -t ext4 /dev/backup/lv_backup 
  [sudo] password for ed12: 
  mke2fs 1.45.6 (20-Mar-2020)  
  Creating filesystem with 1309696 4k blocks and 327680 inodes 
  Filesystem UUID: 7a1f4c55-d0f3-40e6-b899-7f1333d3389c 
  Superblock backups stored on blocks: 
          32768, 98304, 163840, 229376, 294912, 819200, 884736

  Allocating group tables: done 
  Writing inode tables: done
  Creating journal (16384 blocks): done 
  Writing superblocks and filesystem accounting information: done 
```

🌞 **Monter la partition**

```bash
[ed12@backup ~]$ sudo mkdir /backup 
[ed12@backup ~]$ sudo mount /dev/backup//lv_backup /backup
[ed12@backup ~]$ df -h   
  /dev/mapper/backup-lv_backup  4.9G   20M  4.6G   1% /backup

[ed12@backup ~]$ sudo chown ed12:ed12 /backup 
[ed12@backup ~]$ echo "oui je peut ecrir" > /backup/test.txt
[ed12@backup ~]$ cat /backup/test.txt 
oui je peut ecrir 

[ed12@backup ~]$ cat /etc/fstab  

/dev/mapper/rl-root     /                       xfs     defaults        0 0 
UUID=5480819a-273b-4780-9610-b693119c4e04 /boot                   xfs     defaults        0 0  
/dev/mapper/rl-swap     none                    swap    defaults        0 0 
/dev/backup/lv_backup   /backup                 ext4    defaults                          0 0   


[ed12@backup ~]$ sudo umount /backup 
[ed12@backup ~]$ sudo mount -av  
/backup                  : successfully mounted  
```

# Partie 2 : Setup du serveur NFS sur `backup.tp6.linux`

🌞 **Préparer les dossiers à partager**

```bash
[ed12@backup ~]$ mkdir /backup/web.tp6.linux 
[ed12@backup ~]$ mkdir /backup/db.tp6.linux
[ed12@backup ~]$ ls /backup/ 
db.tp6.linux  lost+found  web.tp6.linux 
```

🌞 **Install du serveur NFS**

```bash
[ed12@backup ~]$ sudo dnf install nfs-utils 

Complete! 
```

🌞 **Conf du serveur NFS**
```bash
[ed12@backup ~]$ cat /etc/idmapd.conf | grep Domain 
Domain = tp6.linux 

[ed12@backup ~]$ cat /etc/exports 
/backup/web.tp6.linux 10.5.1.11(rw,no_root_squash)
/backup/db.tp6.linux 10.5.1.12(rw,no_root_squash)
# rw signifie que l'user a le droit de lire et d'ecrir.
```

🌞 **Démarrez le service**
```bash 
[ed12@backup ~]$ sudo systemctl start nfs-server 
[ed12@backup ~]$ systemctl status nfs-server 
   Active: active (exited) since Tue 2021-12-07 18:26:09 CET; 20s ago 

[ed12@backup ~]$ sudo systemctl enable nfs-server 
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.       
```


🌞 **Firewall**

```bash
[ed12@backup ~]$ sudo firewall-cmd --add-port=2049/tcp --permanent 
success
[ed12@backup ~]$ sudo firewall-cmd --reload 
success
[ed12@backup ~]$ sudo systemctl restart nfs-server 
[ed12@backup ~]$ sudo ss -ltpn
LISTEN            0                 64                                   0.0.0.0:2049                               0.0.0.0:*
LISTEN            0                 64                                      [::]:2049                                  [::]:*
```

# Partie 3 : Setup des clients NFS : `web.tp6.linux` et `db.tp6.linux`

🌞 **Install'**

```bash
[ed12@web ~]$ sudo dnf install nfs-utils 
Complete! 
```

🌞 **Conf'**

```bash
[ed12@web ~]$ sudo mkdir /srv/backup 
[ed12@web ~]$ cat /etc/idmapd.conf | grep tp6.linux
Domain = tp6.linux

[ed12@web ~]$ sudo mount -t nfs 10.5.1.13:/backup/web.tp6.linux /srv/backup 
```

🌞 **Montage !**

```bash
[ed12@web ~]$ df -h | grep backup
10.5.1.13:/backup/web.tp6.linux  4.9G   20M  4.6G   1% /srv/backup

[ed12@web ~]$ echo "this is my test" > /srv/backup/test.txt
[ed12@web ~]$ cat /srv/backup/test.txt 
this is my test

[ed12@web ~]$ cat /etc/fstab | grep backup
10.5.1.13:/backup/web.tp6.linux /srv/backup nfs defaults 0 0

[ed12@web ~]$ sudo umount /srv/backup
[ed12@web ~]$ sudo mount -av
/srv/backup              : successfully mounted

```

🌞 **Répétez les opérations sur `db.tp6.linux`**

```bash
[ed12@db ~]$ sudo dnf install nfs-utils 
Complete!

[ed12@db ~]$ sudo mkdir /srv/backup 
[ed12@db ~]$ cat /etc/idmapd.conf | grep tp6.linux
Domain = tp6.linux

[ed12@db ~]$ sudo mount -t nfs 10.5.1.13:/backup/db.tp6.linux /srv/backup 

[ed12@db ~]$ df -h | grep backup
10.5.1.13:/backup/db.tp6.linux  4.9G   20M  4.6G   1% /srv/backup

[ed12@db ~]$ cat /etc/fstab | grep db
10.5.1.13:/backup/db.tp6.linux /srv/backup nfs defaults 0 0

[ed12@db ~]$ sudo umount /srv/backup 
[ed12@db ~]$ sudo mount -av
/srv/backup              : successfully mounted
```














