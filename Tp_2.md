# Prérequis
🌞 **Changer le nom de la machine**

# Partie 1 : SSH

## 1. Installation du serveur
erwan@node1:~$ sudo apt install openssh-server
 The following NEW packages will be installed:
 openssh-server


## 2. Lancement du service SSH
erwan@node1:~$ sudo systemctl status sshd
     Active: active (running) since Mon 2021-11-08 20:41:10 CET; 2min 16s ago

## 3. Etude du service SSH
erwan@node1:~$ sudo systemctl status sshd
ssh.service - OpenBSD Secure Shell server
Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: e>
Active: active (running) since Mon 2021-11-08 20:41:10 CET; 10min ago

erwan@node1:~$ ps -aux |grep ssh
erwan       1029  0.0  0.0   6040   456 ?        Ss   20:38   0:00 /usr/bin/ssh-agent /usr/bin/im-launch startxfce4
root        2333  0.0  0.3  12184  7072 ?        Ss   20:41   0:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
erwan       3031  0.0  0.0  19380   728 pts/0    S+   20:58   0:00 grep --color=auto ssh

erwan@node1:~$ ss -l |grep ssh
 u_str   LISTEN   0        4096             /run/user/1000/gnupg/S.gpg-agent.ssh 26291                                           *               0                             
 u_str   LISTEN   0        10                         /run/user/1000/keyring/ssh 26868                                           * 0                             
u_str   LISTEN   0        128                   /tmp/ssh-nxVxvWBH2qIF/agent.909 26596                                           * 0                             
tcp     LISTEN   0        128                                           0.0.0.0:ssh                                       0.0.0.                0:*                             
 tcp     LISTEN   0        128              [::]:ssh  [::]:* 

erwan@node1:~$ journalctl |grep ssh
 nov. 08 20:40:56 node1.tp2.linux sudo[2032]:    erwan : TTY=pts/0 ; PWD=/home/erwan ; USER=root ; COMMAND=/usr/bin/apt install openssh-server
nov. 08 20:41:10 node1.tp2.linux sshd[2333]: Server listening on 0.0.0.0 port 22.
nov. 08 20:41:10 node1.tp2.linux sshd[2333]: Server listening on :: port 22.
nov. 08 20:51:59 node1.tp2.linux sudo[2955]:    erwan : TTY=pts/0 ; PWD=/home/erwan ; USER=root ; COMMAND=/usr/bin/systemctl status sshd

## 4. Connexion au serveur depuis mon Windows. 
C:\Users\erwan>ssh erwan@192.168.116.4
 erwan@node1:~$

## 5. Modification de la configuration du serveur
erwan@node1:/etc/ssh$ sudo nano sshd_config
erwan@node1:/etc/ssh$ cat sshd_config

 Include /etc/ssh/sshd_config.d/*.conf

 Port 2021

erwan@node1:/etc/ssh$ systemctl restart sshd
erwan@node1:/etc/ssh$ ss -l |more
 tcp     LISTEN   0        128                                              [::]:2021 

## 6. Connexion au port choisit.
C:\Users\erwan>ssh erwan@192.168.116.4 -p 2021
 erwan@192.168.116.4's password:
 Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)

# Partie 2 : FTP

## 1. Installation du service.
erwan@node1:/etc/ssh$ sudo apt install vsftpd
 The following NEW packages will be installed:
 vsftpd

erwan@node1:/etc/ssh$ systemctl status vsftpd
     Active: active (running) since Mon 2021-11-08 21:58:04 CET; 1min 45s ago

## 2. Etude du service FTP.

erwan@node1:/etc/ssh$ systemctl status vsftpd
 Active: active (running) since Mon 2021-11-08 21:58:04 CET; 3min 36s ago

erwan@node1:/etc/ssh$ ps -aux |grep ftp
 root        3733  0.0  0.1   6816  3072 ?        Ss   21:58   0:00 /usr/sbin/vsftpd /etc/vsftpd.conf
erwan       4382  0.0  0.0  19380   728 pts/0    S+   22:02   0:00 grep --color=auto ftp

erwan@node1:/etc/ssh$ ss -l |grep ftp
 tcp     LISTEN   0        32                                                  *:ftp                                             *:* 

erwan@node1:/etc/ssh$ journalctl |grep ftp
 nov. 08 21:57:46 node1.tp2.linux sudo[3557]:    erwan : TTY=pts/0 ; PWD=/etc/ssh ; USER=root ; COMMAND=/usr/bin/apt install vsftpd
nov. 08 21:57:57 node1.tp2.linux groupadd[3621]: group added to /etc/group: name=ftp, GID=133
nov. 08 21:57:57 node1.tp2.linux groupadd[3621]: group added to /etc/gshadow: name=ftp
nov. 08 21:57:57 node1.tp2.linux groupadd[3621]: new group: name=ftp, GID=133
nov. 08 21:57:57 node1.tp2.linux useradd[3627]: new user: name=ftp, UID=124, GID=133, home=/srv/ftp, shell=/usr/sbin/nologin, from=none
nov. 08 21:57:57 node1.tp2.linux usermod[3635]: change user 'ftp' password
nov. 08 21:57:58 node1.tp2.linux chage[3642]: changed password expiry for ftp
nov. 08 21:57:58 node1.tp2.linux chfn[3646]: changed user 'ftp' information
nov. 08 21:58:04 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 08 21:58:04 node1.tp2.linux systemd[1]: Started vsftpd FTP server.

erwan@node1:/var/log$ tail -f -n 50 dpkg.log 
2021-11-08 21:57:56 install vsftpd:amd64 <none> 3.0.3-12

Dans l'explorateur de fichiers:
ftp://192.168.116.4
identifiants :
Pwd :

erwan@node1:/etc$ sudo nano vsftpd.conf
  (# Uncomment this to enable any form of FTP write command.)
write_enable=YES

erwan@node1:~/Documents$ systemctl restart vsftpd

depuis l'explorateur de fichiers copie/past d'un doc nommé words2.txt
dans la vm
erwan@node1:~/Documents$ ls
words2.txt

erwan@node1:/var/log$ sudo nano vsftpd.log 
Mon Nov  8 22:43:13 2021 [pid 4689] [erwan] OK UPLOAD: Client "::ffff:192.168.116.1", "/home/erwan/Documents/words2.txt", 168 bytes, >
Mon Nov  8 22:57:59 2021 1 ::ffff:192.168.116.1 167 /home/erwan/Documents/words3.txt b _ i r erwan ftp 0 * c

erwan@node1:/etc$ sudo nano vsftpd.conf
listen_port=2022

erwan@node1:/etc$ sudo apt install net-tools
The following NEW packages will be installed:
        net-tools
erwan@node1:/etc$ sudo netstat -tulpn |grep vsftpd
tcp6       0      0 :::2022                 :::*                    LISTEN      5664/vsftpd  


# Partie 3 : Creation de mon propre service
erwan@node1:/$ sudo apt install netcat
The following NEW packages will be installed:
 netcat
impossible d'installer netcat sur windows, le fichier est constamment bloqué par windows defender.






  


