# TP 3 : A little script

## I. Script carte d'identité

- Lien script : https://gitlab.com/erwan.durand.73/rendu_de_tp_linux_erwan_durand/-/blob/main/TP_3/idcard.sh

- Execution script:

```bash
erwan@node1:/srv/idcard$ sudo bash idcard.sh      
[sudo] password for erwan:

Machine name: node1.tp4.linux 
OS Ubuntu and kernel version is 5.11.0-40-generic 
IP : 192.168.116.5 
RAM : 1,4 Go/1,9 Go
Disque : 2,5 Go left
Top 5 process by Ram usage :
-  2.0     971 /usr/lib/x86_64-linux-gnu/xfce4/panel/wrapper-2.0 /usr/lib/x86_64-linux-gnu/xfce4/panel/plugins/libpulseaudio-plugin.so 10 16777229 pulseaudio PulseAudio Plugin Adjust the audio volume of the PulseAudio sound system
-  2.1     964 /usr/lib/x86_64-linux-gnu/xfce4/panel/wrapper-2.0 /usr/lib/x86_64-linux-gnu/xfce4/panel/plugins/libwhiskermenu.so 1 16777223 whiskermenu Whisker Menu Show a menu to easily access installed applications
-  2.2    1007 /usr/bin/python3 /usr/bin/blueman-applet
-  3.4     576 /usr/lib/xorg/Xorg -core :0 -seat seat0 -auth /var/run/lightdm/root/:0 -nolisten tcp vt7 -novtswitch
-  4.1     935 xfwm4 --replace
Listening ports : 
- 53 : systemd-resolve
- 22 : sshd
- 631 : cupsd
- 21 : vsftpd
Voici ton image de chat : https://cdn2.thecatapi.com/images/d6v.jpg
```

## II. Script youtube-dl

- Lien script : https://gitlab.com/erwan.durand.73/rendu_de_tp_linux_erwan_durand/-/blob/main/TP_3/yt.sh

- Lien fichier log : https://gitlab.com/erwan.durand.73/rendu_de_tp_linux_erwan_durand/-/blob/main/TP_3/download.log

- Execution script :

```bash
erwan@node1:/srv/yt$ sudo bash yt.sh https://www.youtube.com/watch?v=tbnLqRW9Ef0
/srv/yt/downloads exists.
Video https://www.youtube.com/watch?v=tbnLqRW9Ef0 has been sucessfuly downloaded.
File path : /srv/yt/downloads/1 sec VIDEO/1 sec VIDEO.mp4

erwan@node1:/var/log/yt$ cat download.log
[11/22/21 14:39:35] Video  was downloaded. File path : /srv/yt/downloads//.mp4
[11/22/21 14:41:22] Video https://www.youtube.com/watch?v=EU4nktO6xpk was downloaded. File path : /srv/yt/downloads/Vettel: "I'm gonna touch Hamilton's rear wing"/.mp4
[11/22/21 14:49:15] Video https://www.youtube.com/watch?v=_4Tmqj-uXQs was downloaded. File path : /srv/yt/downloads/Formula 1 In 10 Seconds/.mp4
[11/22/21 14:52:18] Video https://www.youtube.com/watch?v=GWCITkGdki8 was downloaded. File path : /srv/yt/downloads/MAX VERSTAPPEN REAR WING VIBRATION/.mp4
[11/22/21 22:15:36] Video https://www.youtube.com/watch?v=tbnLqRW9Ef0 was downloaded. File path : /srv/yt/downloads/1 sec VIDEO/.mp4
```

### III. Make it a sercice.

- Lien script : https://gitlab.com/erwan.durand.73/rendu_de_tp_linux_erwan_durand/-/blob/main/TP_3/yt-v2.sh

- Lien service : https://gitlab.com/erwan.durand.73/rendu_de_tp_linux_erwan_durand/-/blob/main/TP_3/yt.service





