FILE=/srv/yt/downloads
if [ -d "$FILE" ]; then
    echo "$FILE exists."
else 
    echo "$FILE does not exist."
    exit
fi
youtube_video_url="$1"
nom_video=$(youtube-dl --get-title $youtube_video_url)
youtube-dl -o "/srv/yt/downloads/$nom_video/%(title)s.%(ext)s" $youtube_video_url > /dev/null
youtube-dl --get-description $youtube_video_url >> "/srv/yt/downloads/$nom_video/description"

echo "Video $youtube_video_url has been sucessfuly downloaded."
echo "File path : /srv/yt/downloads/$nom_video/$nom_video.mp4"
echo "[$(date '+%D %T')] Video $youtube_video_url was downloaded. File path : /srv/yt/downloads/$nom_video/$nom_vidoe.mp4" >> "/var/log/yt/download.log"

