PCNAME=$(hostname)
echo "Machine name: $PCNAME"
OSname=$(cat /etc/os-release | head -1 | cut -d '"' -f2)
VerKer=$(uname -r)
echo "OS $OSname and kernel version is $VerKer"
IP=$(hostname -I | cut -d ' ' -f2)
echo "IP : $IP"
MemTot=$(free -mh | grep Mem | cut -d ' ' -f11 | cut -b -3 )
MemAvi=$(free -mh | grep Mem | cut -d ' ' -f46 | cut -b -3 )
echo "RAM : $MemAvi Go/$MemTot Go"
Disk=$(df -h | grep /dev/sda5 | cut -d ' ' -f12 | cut -b -3 )
echo "Disque : $Disk Go left"
process1=$(ps -eo %mem,pid,command | sort | tail -6 | head -1)
process2=$(ps -eo %mem,pid,command | sort | tail -5 | head -1)
process3=$(ps -eo %mem,pid,command | sort | tail -4 | head -1)
process4=$(ps -eo %mem,pid,command | sort | tail -3 | head -1)
process5=$(ps -eo %mem,pid,command | sort | tail -2 | head -1)
echo "Top 5 process by Ram usage :"
echo " - $process1"
echo " - $process2"
echo " - $process3"
echo " - $process4"
echo " - $process5"
echo "Listening ports :"
ss -alnpt -H | while read line
do 
port=$(echo $line | tr -s ' ' | cut -d ' ' -f4 | rev | cut -d ':'  -f1| rev)
nom=$(echo $line | cut -d '"' -f2)
echo " - ${port} : ${nom} "
done
chat=$(curl -s https://api.thecatapi.com/v1/images/search| cut -d '"' -f10)
echo "Voici ton image de chat : $chat"
