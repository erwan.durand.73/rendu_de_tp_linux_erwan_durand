while true; do
        dl_file="$(cat /srv/yt/to_dl)"
        if [[ (-d "/srv/yt/downloads") && (-d "/var/log/yt") && (-n "/srv/yt/downloads") ]]; then
		cat /srv/yt/to_dl | while read line; do 
			url=$line
			vid_name=$(youtube-dl --get-title $url)
	                path="/srv/yt/downloads/$vid_name"
			mkdir "$path"
	                mkdir "$path/description"
	                echo "Video $url has been downloaded."
	                echo "File path : $path.mp4"
	                youtube-dl -o "$path/%(vid_name)s.%(ext)s" "$url" > "/dev/null"
	                youtube-dl -o "$path/description/$vid_name" --write-description --skip-download --youtube-skip-dash-manifest "$url" > "/dev/null"
	                echo "[$(date "+%Y/%m/%d %T")] Video $url has been succesfully downloaded. File path : $path/$vid_name.mp4" >> "/var/log/yt/download.log"
	                sed -i '1d' "/srv/yt/to_dl"
		done
	fi
	sleep 10
done
