# TP4 : Une distribution orientée serveur

## Checklist :

- Fichier de conf 

```bash
[ed12@localhost network-scripts]$ cat ifcfg-enp0s8 
TYPE=Ethernet 
BOOTPROTO=static
NAME=enp0s8 
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.250.1.12
NETMASK=255.255.255.0
```
```bash
[ed12@localhost ~]$ ip a   
     inet 10.250.1.12/24 brd 10.250.1.255 scope global noprefixroute enp0s8 


[ed12@localhost ~]$ systemctl status sshd  
     Active: active (running) since Tue 2021-11-23 16:43:07 CET; 17min ago
```

➜ **Connexion SSH fonctionnelle**

```bash 
[ed12@localhost ~]$ systemctl status sshd
     Active: active (running) since Tue 2021-11-23 16:43:07 CET; 7h ago


➜ **Accès internet**

```bash
[ed12@localhost ~]$ ping 1.1.1.1
     64 bytes from 1.1.1.1: icmp_seq=1 ttl=54 time=30.1 ms

[ed12@localhost ~]$ ping ynov.com
     PING ynov.com (92.243.16.143) 56(84) bytes of data.
     64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=51 time=27.0 ms 
```
➜ **Nommage de la machine**

```bash
[ed12@localhost ~]$ sudo hostname node1.tp4.linux 
[ed12@node1 ~]$ hostname
     node1.tp4.linux 

[ed12@node1 ~]$ sudo cat /etc/hostname
     node1.tp4.linux
```


## 2. NGINX :

🌞 **Installez NGINX**

```bash
[ed12@node1 ~]$  sudo dnf install nginx 
     <jsp quelle ligne choisir y'en a bcp en vrai>

[ed12@node1 ~]$ sudo systemctl start nginx
[sudo] password for ed12:

[ed12@node1 ~]$ systemctl status nginx
     Active: active (running) since Tue 2021-11-23 19:37:35 CET; 1min 7s ago
```
🌞 **Analysez le service NGINX**

```bash
[ed12@node1 ~]$ ps -aux | grep nginx
     root       25887  0.0  0.2 119152  2164 ?        Ss   19:37   0:00 nginx: master process /usr/sbin/nginx 
     nginx      25888  0.0  0.9 151804  8240 ?        S    19:37   0:00 nginx: worker process 


[ed12@node1 ~]$ sudo ss -ltpn | grep nginx 
     LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=25888,fd=8),("nginx",pid=25887,fd=8))
     LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=25888,fd=9),("nginx",pid=25887,fd=9))

[ed12@node1 ~]$ cat /etc/nginx/nginx.conf 
     root         /usr/share/nginx/html


[ed12@node1 ~]$ ls -la /usr/share/nginx/html/                                                                                        
     drwxr-xr-x. 2 root root   99 Nov 23 18:02.
     drwxr-xr-x. 4 root root   33 Nov 23 18:02 ..                          
     -rw-r--r--. 1 root root 3332 Jun 10 11:09 404.html
     -rw-r--r--. 1 root root 3404 Jun 10 11:09 50x.html
     -rw-r--r--. 1 root root 3429 Jun 10 11:09 index.html
     -rw-r--r--. 1 root root  368 Jun 10 11:09 nginx-logo.png
     -rw-r--r--. 1 root root 1800 Jun 10 11:09 poweredby.png  
```

🌞 Configurez le firewall pour autoriser le trafic vers le service NGINX

```bash
[ed12@node1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent 
[sudo] password for ed12:
     success  

[ed12@node1 ~]$ sudo firewall-cmd --reload
     success   
```
🌞 Test depuis le terminal.

```bash
[ed12@node1 ~]$ curl http://10.250.1.12:80  
   <div class="content">                                                                                           <p>                                                                                                                                                      This page is used to test the proper operation of the 
    <strong>nginx</strong> HTTP server after it has been installed. If you
    can read this page, it means that the web server installed at this site 
    is working properly.  
```
🌞 Changer le port d'écoute

```bash
[ed12@node1 ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
[sudo] password for ed12:
     success

[ed12@node1 ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
     success 

[ed12@node1 ~]$ sudo firewall-cmd --reload
     success 

[ed12@node1 ~]$ systemctl restart nginx

[ed12@node1 ~]$ systemctl status nginx
     Active: active (running) since Tue 2021-11-23 23:15:20 CET; 13s ago 


[ed12@node1 ~]$ sudo ss -ltpn | grep nginx 
     LISTEN 0      128          0.0.0.0:8080        0.0.0.0:*    users:(("nginx",pid=25888,fd=8),("nginx",pid=25887,fd=8))
     LISTEN 0      128             [::]:8080           [::]:*    users:(("nginx",pid=25888,fd=9),("nginx",pid=25887,fd=9))

PS C:\Users\erwan> curl http://10.250.1.12:8080
     Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"> 
```

Nouveau user
```bash
[ed12@node1 ~]$ sudo useradd web -m -s /bin/sh -u 2000
     [sudo] password for ed12: 
     Sorry, try again.
     [sudo] password for ed12: 


[ed12@node1 ~]$ sudo passwd web
     Changing password for user web.
     New password: 
     BAD PASSWORD: The password fails the dictionary check - it is too simplistic/systematic 
     Retype new password:
     passwd: all authentication tokens updated successfully.


[ed12@node1 ~]$ ps -ef | grep nginx
root        4859       1  0 18:15 ?        00:00:00 nginx: master process /usr/sbin/nginx
web         4860    4859  0 18:15 ?        00:00:00 nginx: worker process
ed12        4865    2004  0 18:16 pts/0    00:00:00 grep --color=auto nginx
 
```
🌞 Changer l'emplacement de la racine Web

```bash
[ed12@node1 /]$ sudo mkdir /var/www
[ed12@node1 /]$ [zqo@node1 /]$ sudo mkdir /var/www
[ed12@node1 /]$ cd /var/www
[ed12@node1 var]$ sudo chmod -R 777 www/
[ed12@node1 var]$ sudo su - web
[web@node1 ~]$ cd /var/www/
[web@node1 www]$ mkdir super_site_web

[web@node1 www]$ ls -l  
     total 0
     drwxrwxr-x. 2 web web 6 Nov 24 00:11 super_site_web 

[web@node1 www]$ ls -la super_site_web/
     drwxrwxr-x. 2 web  web   6 Nov 24 00:11 . 
     drwxrwxrwx. 3 root root 28 Nov 24 00:11 ..

[ed12@node1 var]$ cat /etc/nginx/nginx.conf
    server {
        listen       8080 default_server;
        listen       [::]:8080 default_server;
        server_name  _;
        root         /var/www/super_site_web;


[ed12@node1 var]$ curl http://10.250.1.12:8080/
<!DOCTYPE html>
<html>
<h1>toto</h1>
</html>
```



















